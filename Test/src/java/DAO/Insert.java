/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import GenericDAO.ObjetBDD;
import Models.Personne;
import annotation.UrlAnnotation;
import connexion.Connexion;
import fonction.Utilitaire;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import view.ModelView;

/**
 *
 * @author ramar
 */
public class Insert {
        private static Connexion con = new Connexion();
    
    @UrlAnnotation.url(name="/Insert", classe="Personne")
    public ModelView InsertPersonne(ObjetBDD obj, HttpServletRequest request) {
        HashMap<String, Object> data = new HashMap<String, Object>();
        try {
            HashMap<String, Object> params = new Utilitaire().Parameters(obj, request);
            ArrayList<ObjetBDD> dt = new Personne().Select(con);
            data.put("Liste",dt);
            new Utilitaire().Insert(obj, params);
            obj.save(new Connexion()); 
        }
        catch(Exception e) { throw e; }
        finally {
            return new ModelView(data,"after.jsp");
        }
    }
}
