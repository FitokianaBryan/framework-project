CREATE DATABASE GServe;

CREATE TABLE Personne (
    ID SERIAL PRIMARY KEY,
    Nom VARCHAR(200) NULL,
    Prenom VARCHAR(200) NULL
);
INSERT INTO Personne(Nom,Prenom) VALUES('RAKOTO','Jean'),('RASOA','Jeanne');

CREATE TABLE Commune(
    ID SERIAL PRIMARY KEY,
    Nom VARCHAR(150) NULL
);
INSERT INTO Commune(Nom) VALUES('Antananarivo Renivohitra'),('Itaosy')